@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
					<form id="form-add-list" method="post">
						<div class="input-group">
							<input type="text" class="form-control" name="name" placeholder="Your list name...">
							<span class="input-group-btn">
								<button class="btn btn-secondary" type="submit">Add</button>
							</span>
						</div>
					</form>
				</div>
				<div class="panel-body">
                    @include('shared.alerts')
                    
					<ul class="todolists list-group">
					@foreach ($todolists as $todolist)
					  <li data-todolist_id="{{$todolist->todolist_id}}" class="list-group-item">
						  {{$todolist->name}}
						  <button class="pull-right button-export-todolist btn-warning btn-xs" type="button">Export</button> 
						  <button class="pull-right button-delete-todolist  btn-danger btn-xs" type="button">Delete</button>
						  <button class="pull-right button-expand-todolist	btn btn-info btn-xs" type="button">Expand</button>
						  <div style="display: none" class="tasks">
							<table class="table">
							<thead>
								<tr>
								  <th>Task</th>
								  <th>Checked</th>
								  <th>Operation</th>
								</tr>
							</thead>
							<tbody>
								@foreach($todolist->tasks as $task)
								<tr class="task" data-task_id="{{$task->task_id}}">
								  <td><input type="text" name="name" class="form-control" value="{{$task->name}}"/></td>
								  <td>
									 @if($task->checked)
										<input checked type="checkbox" name="checked" class="form-control" />
									 @else
										<input type="checkbox" name="checked" class="form-control" />	
									 @endif
								  </td>
								  <td><button type="button" class="button-delete-task btn-danger btn-xs">Delete</button></td>
								</tr>
								@endforeach
							</tbody>
							<button class="button-add-task btn-info btn-xs">Add task</button>
							</table>
						  </div>
					  </li>
					  @endforeach	
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/notify/notify.min.js') }}" type="text/javascript"></script>
<script>
	$(document).on('click', '.button-export-todolist', function(){
		var todolist_id = $(this).parents('li').data('todolist_id');
		var url = 'todolist/' + todolist_id + '/excel';
		var win = window.open(url, '_blank');
		win.focus();
	});
	
	$(document).on('change', '.task input', function(){
		var task_id = $(this).parents('tr').data('task_id');
		//Handle the checkbox..
		var data = {};
		if($(this).attr('type') == 'checkbox'){
			if($(this).is(':checked')){
				data[ $(this).attr('name') ] = 1;
			}else{
				data[ $(this).attr('name') ] = 0;
			}
		}else{
			data = $(this).serialize();
		}
		
		$.ajax({
			url 	: 'todolist/task/' + task_id + '/edit',
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			},
			//context : this,
			method	: 'post',
			dataType: 'json',
			data	: data,
			success	: function(json){
				notify(json);
			},
			error	: function(error){
				console.log(error);
			}
		});
		
	});
	
	$(document).on('click', '.button-expand-todolist', function(){
		$(this).parents('li').find('.tasks').toggle()
	});
	
	$(document).on('click', '.button-delete-task', function(){
		var task_id = $(this).parents('tr').data('task_id');
		$.ajax({
			url 	: 'todolist/task/' + task_id + '/delete',
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			},
			context : this,
			method	: 'delete',
			dataType: 'json',
			success	: function(json){
				notify(json);
				if(json.success){
					$(this).parent().parent().remove();
				}
			},
			error	: function(error){
				console.log(error);
			}
		});
		
	});
	
	$(document).on('submit', '#form-add-list', function(event){
		event.preventDefault();
		
		$.ajax({
			url 	: "{{URL::to('todolist/create')}}",
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			},
			method	: 'post',
			dataType: 'json',
			data	: $(this).serialize(),
			success	: function(json){
				notify(json);
				if(json.error){
					console.log(json.error);//todo
				}else{
					var html = '';
					html += '<li data-todolist_id="'+json.todolist_id+'" class="list-group-item">';
					html += json.name;
					html += '<button type="button" class="pull-right button-export-todolist btn-warning btn-xs">Export</button>';
					html += '<button type="button" class="pull-right button-delete-todolist  btn-danger btn-xs">Delete</button>';
					html += '<button type="button" class="pull-right button-expand-todolist	btn btn-info btn-xs">Expand</button>';
					html += '<div class="tasks" style="display: none;">';
					html += '<button class="button-add-task btn-info btn-xs">Add task</button>';
					html += '<table class="table">';
					html += '<thead>';
					html += '<tr>';
					html += '<th>Task</th>';
					html += '<th>Checked</th>';
					html += '<th>Operation</th>';
					html += '</tr>';
					html += '</thead>';
					html += '<tbody>';
					html += '</tbody>';
					html += '</table>';
					html += '</div>';
					html += '</li>';
					
					$('.todolists').append(html);
				
					//Clear the input field
					$('#form-add-list input,#form-add-list textarea').val('');
				}
			},
			error	: function(err){
				console.log(err);
			}
		})
	});
	
	$(document).on('click', '.button-delete-todolist', function(){
		var todolist_id = $(this).parents('li').data('todolist_id');
		
		$.ajax({
			url 		: "todolist/" + todolist_id + '/delete',
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			},
			method		: 'delete',
			dataType	: 'json',
			success		: function(json){
				notify(json);
				if(json.success){
					$('li[data-todolist_id="'+todolist_id+'"]').remove();
				}
			},
			error		: function(error){
				console.log(error);
			}
		});
	});
	
	$(document).on('click', '.button-add-task', function(){
		var todolist_id = $(this).parents('li').data('todolist_id');
		$.ajax({
			url 		: "todolist/" + todolist_id + '/task/create',
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			},
			dataType	: 'json',
			success		: function(json){
				notify(json);
				if(json.success){
					var html = '';
					html += '<tr class="task" data-task_id="'+json.task_id+'">';
					html += '<td><input name="name" class="form-control" type="text" value=""></td>';
					html += '<td><input name="checked" class="form-control" type="checkbox"></td>';
					html += '<td><button class="button-delete-task btn-danger btn-xs" type="button">Delete</button></td>';
					html += '</tr>';
					
					$('li[data-todolist_id="'+todolist_id+'"] tbody').append(html);
				}
				
			},
			error		: function(error){
				console.log(error);
			}
		});
	});
	
	//Notify.js 
	function notify(data){
		if(data.success){
			$.notify(data.success, "success");
		}else if(data.error){
			$.notify(data.error, "error");
		}
	}
	</script>
@endsection
		