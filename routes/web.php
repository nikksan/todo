<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
//List
Route::get('/', 'ToDoListController@index');
Route::get('/home', 'ToDoListController@index');
Route::get('/todolist', 'ToDoListController@index');
Route::post('/todolist/create', 'ToDoListController@create');
Route::delete('/todolist/{todolist_id}/delete', 'ToDoListController@delete');
Route::get('/todolist/{todolist_id}/excel', 'ToDoListController@excel');

//Task
Route::get('/todolist/{todolist_id}/task/create', 'TaskController@create');
Route::delete('/todolist/task/{task_id}/delete', 'TaskController@delete');
Route::post('/todolist/task/{task_id}/edit', 'TaskController@edit');