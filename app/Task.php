<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    //Change primary key
	protected $primaryKey = 'task_id';
	protected $table = 'task';
	
	public static function addTask( $data ){
		return \DB::table('task')->insertGetId($data);
	}
	
	public static function deleteTask( $task_id ){
		return \DB::table('task')->where('task_id', '=', $task_id)->delete();
	}
	
	public static function editTask($task_id , $data){
		return \DB::table('task')->where('task_id', '=', $task_id )->update($data);
	}
	
	public static function getTasks($todolist_id){
		return \DB::table('task')->where('todolist_id', $todolist_id )->get();
	}
	
}
