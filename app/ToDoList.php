<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ToDoList extends Model
{
    //Change primary key
	protected $primaryKey = 'todolist_id';
	protected $table = 'todolist';
	
	public static function getToDoList($todolist_id){
		return \DB::table('todolist')->where('todolist_id', '=', $todolist_id)->where('user_id', Auth::id())->first();
	}
	
	public static function getToDoLists(){
		return \DB::table('todolist')->where('user_id', Auth::id())->get();
	}
	
	public static function addToDoList($data){
		return \DB::table('todolist')->insertGetId( $data );
	}
	
	public static function updateToDoList($todolist_id, $data){
		return \DB::table('todolist')->where('todolist_id', '=', $todolist_id )->update( $data );
	}
	
	public static function deleteList($todolist_id ){
		return \DB::table('todolist')->where('todolist_id', '=', $todolist_id )->delete();
	}
	
}
