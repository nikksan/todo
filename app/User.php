<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

	//Change primary key
	protected $primaryKey = 'user_id';
	//protected $table = 'user'; //laravel is ignosring that..
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role'
    ];
	
	public static function hasPermissionToList($todolist_id){
		return DB::table('todolist')->where(array(
			'todolist_id' 		=> $todolist_id ,
			'todolist.user_id'	=> Auth::id()
			))->count();
	}
	
	public static function hasPermissionToTask($task_id){
		return DB::table('task')
				->join('todolist', 'todolist.todolist_id', '=', 'task.todolist_id')
				->where(array(
				'task.task_id' 		=> $task_id ,
				'todolist.user_id'	=> Auth::id()
				))
				->count();
	}
}
