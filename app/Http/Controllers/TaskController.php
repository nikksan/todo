<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

	
	public function create( $todolist_id ){
		$json = array();
		
		if(!\App\User::hasPermissionToList($todolist_id)){
			$json['error'] = 'You dont have permission!';
		}else{
			$data = array(
				'todolist_id' => $todolist_id
			);
			$task_id = \App\Task::addTask($data);
			if(!$task_id){
				$json['error'] = 'Error adding to database!';
			}else{
				$json['success'] = 'Successfully added a task!';
				$json['task_id'] = $task_id;
			}
		}
		
		return response()->json($json);
	}
	
	public function delete( $task_id ){
		$json = array();
		
		if(!\App\User::hasPermissionToTask($task_id)){
			$json['error'] = 'You dont have permission!';
		}else{
			$affected_rows = \App\Task::deleteTask($task_id);
			if(!$affected_rows){
				$json['error'] = 'Error deleting task!';
			}else{
				$json['success'] = 'Successfully deleted a task!';
			}
		}
		
		return response()->json($json);
	}
	
	
	public function edit($task_id){
		$json = array();
		
		if(!\App\User::hasPermissionToTask($task_id)){
			$json['error'] = 'You dont have permission!';
		}else{
			$affected_rows = \App\Task::editTask($task_id, $_POST );//couldnt find the 'Laravel way'
			if(!$affected_rows){
				$json['error'] = 'Error editing task!';
			}else{
				$json['success'] = 'Successfully edited a task!';
			}
		}
		
		return response()->json($json);
	}
	
	
}
