<?php
	
	namespace App\Http\Controllers;
	
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	
	//Spout
	use Box\Spout\Writer\WriterFactory;
	use Box\Spout\Common\Type;
	
	class ToDoListController extends Controller
	{
		/**
			* Create a new controller instance.
			*
			* @return void
		*/
		public function __construct(){
			$this->middleware('auth');
		}
		
		
		
		/**
			* Show the application dashboard.
			*
			* @return \Illuminate\Http\Response
		*/
		
		
		public function index(){
			$data = array();
			$data['todolists'] = array();
			$todolists =  \App\ToDoList::getToDoLists();
			foreach($todolists as $todolist){
				$list = new \stdClass();
				$list->todolist_id 	= $todolist->todolist_id;
				$list->name			= $todolist->name;
				$list->tasks		= \App\Task::getTasks( $todolist->todolist_id );
				
				$data['todolists'][] = $list;
			}
			
			
			//$request->session()->flash('error', 'Task was not successful!');
			return view('pages.todolist.list', $data);
		}
		
		public function create(Request $request){
			$json = array();
			if(!$request->input('name')){
				$json['error'] = 'Please supply a name for your list!';
				}else{
				$data = array(
				'name' 		=> $request->input('name') ,
				'user_id'	=> Auth::id()
				);
				$todolist_id = \App\ToDoList::addToDoList($data);
				if(!$todolist_id){
					$json['error'] = 'Error adding your list!';
					}else{
					$json['success'] = 'Successfully created list.';
					$json['todolist_id'] = $todolist_id;
					$json['name']		= $request->input('name');
				}
			}
			
			return response()->json($json);
		}
		
		public function delete(Request $request, $todolist_id){
			$json = array();
			if(!\App\User::hasPermissionToList($todolist_id)){
				$json['error'] = 'You dont have permission';
				}else{
				$affected_rows = \App\ToDoList::deleteList( $todolist_id );
				if(!$affected_rows){
					$json['error'] = '0 records deleted.';
					}else{
					$json['success'] = 'List was succesfully deleted';
				}
			}
			
			return response()->json($json);
		}
		
		public function excel($todolist_id){
			if(\App\User::hasPermissionToList($todolist_id)){
				$tasks = \App\Task::getTasks($todolist_id);
				require_once base_path('/vendor/spout/src/Spout/Autoloader/autoload.php');
				$writer = WriterFactory::create(Type::XLSX);
				
				$todolist = \App\ToDoList::getToDoList( $todolist_id );
				$writer->openToBrowser(sprintf('%s.xlsx', $todolist->name ));
				//Header row
				$writer->addRow( array('Task', 'Date Added', 'Checked') );
				
				//Tasks
				foreach($tasks as $task){
					$row = array(
						'task' 			=> $task->name ,
						'date_added'	=> date('F j, Y, g:i a', strtotime( $task->date_added )),
						'checked'		=> $task->checked ? 'Yes' : 'No'
					);
					$writer->addRow( $row );
				}
				$writer->close();
			}else{
				return redirect('/')->with('error', 'You dont have permission to view this list.');
			}
		}
		
	}
